---?color=linear-gradient(to right, #918ef4, #306bac) 
@title[The Lab Requirements]

@snap[north-west]
### The Aim...
@snapend

@snap[west text-white span-75]
</br>
<ul class="split-screen-list">
    <li>Provide a private linux environment for class teaching</li>
    <li>Each student in a class is provided with:</li>
    <ul>
        <li>A persistent and private Linux environment for the duration of the course</li>
        <ul>
            <li>storage (RW)</li>
            <li>persistent (unique) login</li>
            <li>Access to class data (RO)</li>
        </ul>
        <li>access to the same amount of resources (CPU, RAM, DISK)</li>
        <li>access to software specific to the course</li>
        <li>24/7 access</li>
    </ul>
    <li>The tutor/sysadmin requires:</li>
    <ul>
        <li>updateable images (in-course updates/bug-fixes)</li>
        <ul>
            <li>(without losing persistent storage)</li>
        </ul>
        <li>a reproducible environment</li>
        <li>resilience</li>
    </ul>        
</ul>
@snapend

@snap[east split-screen-byline span-30]
perl</br>
X</br>
R</br>
Picard</br>
FastQC</br>
Fastx_toolkit</br>
cutadapt</br>
TrimGalore</br>
trimmomatic</br>
velvet</br>
VelvetOptimiser</br>
Spades</br>
QUAST</br>
NCBI-blast+</br>
Prokka</br>
HMMER</br>
BEDTools</br>
Barrnap</br>
RNAmmer</br>
FastTree</br>
snippy</br>
minimap2</br>
snp-sites</br>
EMBOSS</br>
seqtk</br>
vt</br>
samtools</br>
bcftools</br>
freebayes</br>
qiime2018
@snapend

